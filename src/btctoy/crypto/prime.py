from functools import (
    lru_cache,
)


@lru_cache
def is_prime(n: int) -> bool:
    return miller_rabin(n, 40)


def miller_rabin(n: int, k: int) -> bool:
    import random

    

    if n <= 1:
        return False

    if n == 2 or n == 3:
        return True

    if n % 2 == 0:
        return False

    r, s = 0, n - 1
    while s % 2 == 0:
        r += 1
        s //= 2
    for _ in range(k):
        a = random.randrange(2, n - 1)  # noqa: S311
        x = pow(a, s, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True
