from __future__ import (
    annotations,
)

from typing import (
    BinaryIO,
)

from btctoy.codec import (
    encode_varint,
    int_to_little_endian,
    little_endian_to_int,
    read_varint,
)
from btctoy.script.op import (
    OP_CODE_FUNCTIONS,
    OP_CODE_NAMES,
)
from btctoy.utils.logging import (
    get_logger,
)


# tag::source1[]
def p2pkh_script(h160: bytes) -> Script:
    """Takes a hash160 and returns the p2pkh ScriptPubKey"""
    return Script([0x76, 0xA9, h160, 0x88, 0xAC])


# end::source1[]


LOGGER = get_logger()


class Script:
    def __init__(self, cmds: list[int | bytes] | None = None) -> None:
        if cmds is None:
            self.cmds = []
        else:
            self.cmds = cmds

    def __repr__(self) -> str:
        result = []
        for cmd in self.cmds:
            if type(cmd) == int:
                if OP_CODE_NAMES.get(cmd):
                    name = OP_CODE_NAMES.get(cmd)
                else:
                    name = "OP_[{}]".format(cmd)
                result.append(name)
            else:
                result.append(cmd.hex())
        return " ".join(result)

    def __add__(self, other: Script) -> Script:
        return Script(self.cmds + other.cmds)

    @classmethod
    def parse(cls, s: BinaryIO) -> Script:
        length = read_varint(s)
        cmds = []
        count = 0
        while count < length:
            current = s.read(1)
            count += 1
            current_byte = current[0]
            if current_byte >= 1 and current_byte <= 75:
                n = current_byte
                cmds.append(s.read(n))
                count += n
            elif current_byte == 76:
                data_length = little_endian_to_int(s.read(1))
                cmds.append(s.read(data_length))
                count += data_length + 1
            elif current_byte == 77:
                data_length = little_endian_to_int(s.read(2))
                cmds.append(s.read(data_length))
                count += data_length + 2
            else:
                op_code = current_byte
                cmds.append(op_code)
        if count != length:
            raise SyntaxError("parsing script failed")
        return cls(cmds)

    def raw_serialize(self) -> bytes:
        result = b""
        for cmd in self.cmds:
            if type(cmd) == int:
                result += int_to_little_endian(cmd, 1)
            else:
                
                length = len(cmd)
                
                if length < 75:
                    
                    result += int_to_little_endian(length, 1)
                elif length > 75 and length < 0x100:
                    result += int_to_little_endian(76, 1)
                    result += int_to_little_endian(length, 1)
                elif length >= 0x100 and length <= 520:
                    result += int_to_little_endian(77, 1)
                    result += int_to_little_endian(length, 2)
                else:
                    raise ValueError("too long an cmd")
                result += cmd
        return result

    def serialize(self) -> bytes:
        result = self.raw_serialize()
        total = len(result)
        return encode_varint(total) + result

    def evaluate(self, z: int) -> bool:
        cmds = self.cmds[:]
        stack = []
        altstack = []
        while len(cmds) > 0:
            cmd = cmds.pop(0)
            if type(cmd) == int:
                # do what the opcode says
                operation = OP_CODE_FUNCTIONS[cmd]
                if cmd in (99, 100):
                    # op_if/op_notif require the cmds array
                    if not operation(stack, cmds):
                        LOGGER.info("bad op: {}".format(OP_CODE_NAMES[cmd]))
                        return False
                elif cmd in (107, 108):
                    # op_toaltstack/op_fromaltstack require the altstack
                    if not operation(stack, altstack):
                        LOGGER.info("bad op: {}".format(OP_CODE_NAMES[cmd]))
                        return False
                elif cmd in (172, 173, 174, 175):
                    # these are signing operations, they need a sig_hash
                    # to check against
                    if not operation(stack, z):
                        LOGGER.info("bad op: {}".format(OP_CODE_NAMES[cmd]))
                        return False
                else:
                    if not operation(stack):
                        LOGGER.info("bad op: {}".format(OP_CODE_NAMES[cmd]))
                        return False
            else:
                # add the cmd to the stack
                stack.append(cmd)
        if len(stack) == 0:
            return False
        if stack.pop() == b"":
            return False
        return True
